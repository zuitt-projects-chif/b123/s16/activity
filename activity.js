function displayMultiplicationTable() {
  for (let i = 1; i <= 10; i++) {
    const result = 5 * i;
    console.log(`5 x ${i} = ${result}`);
  }
}
displayMultiplicationTable();

function removeAandE(str) {
  let filteredString = "";
  for (let i = 0; i < str.length; i++) {
    const letter = str[i].toLowerCase();
    filteredString += letter !== "a" && letter !== "e" ? str[i] : "";
  }
  return filteredString;
}
const testString1 = removeAandE("general meeting");
console.log(testString1); // gnrl mting
const testString2 = removeAandE("commitee");
console.log(testString2); // commit
const testString3 = removeAandE("jolibee burger");
console.log(testString3); // jolib burgr

// const removeAandE = (str) => {
//   let filteredString = "";
//   for (char of str) {
//     const lcChar  = char.toLowerCase();
//     filteredString += lcChar !== "a" && lcChar !== "e" ? char : "";
//   }
//   return filteredString;
// };
